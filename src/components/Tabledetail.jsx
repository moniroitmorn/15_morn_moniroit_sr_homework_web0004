import React, { Component } from "react";
import swal from "sweetalert2";
export class Tabledetail extends Component {
  handleSubmit = (item) => {
    swal.fire({
      html:
        "ID : " +
        item.id +
        "<br/>" +
        "Email: " +
        item.email +
        "<br/>" +
        "UserName: " +
        item.userName +
        "<br/>" +
        "Age: " +
        item.age,
    });
  };

  render() {
    return (
      <div>
        <div class="relative overflow-x-auto">
          <div class="relative overflow-x-auto">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
              <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" class="px-6 py-3">
                    Number
                  </th>
                  <th scope="col" class="px-6 py-3">
                    Email
                  </th>
                  <th scope="col" class="px-6 py-3">
                    UserName
                  </th>
                  <th scope="col" class="px-6 py-3">
                    Age
                  </th>
                  <th scope="col" class="px-6 py-3 ">
                    Action
                  </th>
                </tr>
              </thead>

              <tbody>
                {this.props.data.map((item) => (
                  <tr className="even:bg-red-500 bg-slate-300" key={item.id}>
                    <td className="text-2xl text-black text-start">
                      {item.id}
                    </td>
                    <td className="text-2xl text-black text-">
                      {item.email == "" ? "null" : item.email}
                    </td>
                    <td className="text-2xl text-black align-middle">
                      {item.userName == "" ? "null" : item.userName}
                    </td>
                    <td className="text-2xl text-black align-middle">
                      {item.age == "" ? "null" : item.age}
                    </td>
                    <td>
                      <button
                        type="submit"
                        class=" gap-1 text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-normal rounded-lg text-sm w-20 sm:w-20 px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                        onClick={() => this.props.handleChangeStatus(item.id)}
                      >
                        {item.takeStatus}
                      </button>
                      <button
                        onClick={() => this.handleSubmit(item)}
                        type="submit"
                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-normal rounded-lg text-sm w-20 sm:w-20 px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        Show
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Tabledetail;
