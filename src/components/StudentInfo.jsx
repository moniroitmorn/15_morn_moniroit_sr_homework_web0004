import React, { Component } from "react";
import Tabledetail from "./Tabledetail";

export class StudentInfo extends Component {
  constructor() {
    super();
    this.state = {
      studentInfo: [
        // { id: 1, email: "moniroit@gmail.com", userName: "fdf", age: 23 },
      ],
      takeEmail: "",
      takeUsername: "",
      takeAge: "",
      takeStatus: "Pending",
    };
  }

  handleChangeEmail = (e) => {
    this.setState({
      takeEmail: e.target.value,
    });
  };

  handleChangeUsername = (e) => {
    this.setState({
      takeUsername: e.target.value,
    });
  };

  handleChangeAge = (e) => {
    this.setState({
      takeAge: e.target.value,
    });
  };

  onSubmit = (e) => {
    const newObj = {
      id: this.state.studentInfo.length + 1,
      email: this.state.takeEmail,
      userName: this.state.takeUsername,
      age: this.state.takeAge,
      takeStatus: this.state.takeStatus,
    };
    this.setState({
      studentInfo: [...this.state.studentInfo, newObj],
      takeEmail : "",
      takeUsername : "",
      takeAge : "",
      takeStatus : "Pending",
    });
      e.preventDefault();
    };

    handleChangeStatus= (id)=>{
       this.state.studentInfo.map(ids=>{
        if(id===ids.id){
          if(ids.takeStatus==="Pending"){
            ids.takeStatus= "Done"
          }else{
            ids.takeStatus= "Pending"
          }

        }
      })
      // this.setState({
      //   studentInfo: this.state.studentInfo
      // })
      
    }

  render() {
    return (
      <div>
        {/* <form> */}
        <div className="text-center text-4xl bg-red-300 text-fuchsia-500 font-extrabold">
          <span>Please Fill Your </span>
          <span> Information </span>
        </div>

        <label for="email-address-icon" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-2xl">Email</label>
<div class="relative">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
  </div>
  <input type="text" id="email-address-icon" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
   name="email" 
   onChange={this.handleChangeEmail}
  />
</div>

<label for="email-address-icon" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-2xl">UserName</label>
<div class="relative">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z"/></svg>
  </div>
  <input type="text" id="email-address-icon" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
   name="userName" onChange={this.handleChangeUsername}
  />
</div>

<label for="email-address-icon" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-2xl">Age</label>
<div class="relative">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M47.6 300.4L228.3 469.1c7.5 7 17.4 10.9 27.7 10.9s20.2-3.9 27.7-10.9L464.4 300.4c30.4-28.3 47.6-68 47.6-109.5v-5.8c0-69.9-50.5-129.5-119.4-141C347 36.5 300.6 51.4 268 84L256 96 244 84c-32.6-32.6-79-47.5-124.6-39.9C50.5 55.6 0 115.2 0 185.1v5.8c0 41.5 17.2 81.2 47.6 109.5z"/></svg>
  </div>
  <input type="text" id="email-address-icon" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
   name="Age" onChange={this.handleChangeAge}
  />
</div>

        <button
          type="submit"
          class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          onClick={() => this.onSubmit()}
        >
          Submit
        </button>
        {/* </form> */}

        {/* Table Data */}
        <this.handleChangeStatus />
        <Tabledetail data={this.state.studentInfo} handleChangeStatus= {this.handleChangeStatus}/>
      </div>
    );
  }
}

export default StudentInfo;
